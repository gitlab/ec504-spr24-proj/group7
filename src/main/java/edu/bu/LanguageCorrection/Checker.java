package edu.bu.LanguageCorrection;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.zip.Inflater;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;

public class Checker {
    public void analyze(String text, String metadataPath) {
        List<String> sentences = TextProcessor.extractSentences(text);

        TrieNode detector = loadFile(metadataPath);

        Map<String, Float> sentenceScores = new HashMap<>();
        Map<String, Float> phraseScores = new HashMap<>();

        for (String sentence : sentences) {
            sentence = sentence.replaceAll("[^a-zA-Z0-9\\s]", "");
            // System.out.println("Analyzing sentence: " + sentence);
            List<String> phrases = TextProcessor.extractPhrases(sentence, 2, 3);

            // Calculate perplexity (score) for each phrase
            for (String phrase : phrases) {
                // System.out.println("Analyzing phrase: " + phrase);
                float perplexity = detector.perplexity(phrase);
                if (perplexity > 100.0) {
                    phraseScores.put(phrase, 100f);
                } else {
                    phraseScores.put(phrase, perplexity);
                }
            }

            // Calculate average perplexity for the sentence
            float sentenceScore = detector.perplexity(sentence);
            sentenceScores.put(sentence, sentenceScore);
        }

        // Output results in JSON format
        System.out.println("{");
        System.out.println("\"sentences\": " + mapToJson(sentenceScores) + ",");
        System.out.println("\"phrases\": " + mapToJson(phraseScores));
        System.out.println("}");
    }

    private static String mapToJson(Map<String, Float> map) {
        StringBuilder jsonBuilder = new StringBuilder("{");
        for (Map.Entry<String, Float> entry : map.entrySet()) {
            jsonBuilder.append("\"" + entry.getKey() + "\": " + entry.getValue() + ",");
        }
        jsonBuilder.deleteCharAt(jsonBuilder.length() - 1); // remove last comma
        jsonBuilder.append("}");

        return jsonBuilder.toString();
    }

    private static byte[] decompress(byte[] compressedData) {
        Inflater decompressor = new Inflater();
        decompressor.setInput(compressedData);

        ByteArrayOutputStream bos = new ByteArrayOutputStream(compressedData.length);

        byte[] buf = new byte[1024];
        try {
            while (!decompressor.finished()) {
                int count = decompressor.inflate(buf);
                bos.write(buf, 0, count);
            }
            decompressor.end();
            return bos.toByteArray();
        } catch (Exception e) {
            System.err.println("Error decompressing data: " + e.getMessage());
            return new byte[0];
        }
    }

    private TrieNode loadFile(String filePath) {
        TrieNode trie = new TrieNode();
        try (FileInputStream fis = new FileInputStream(filePath)) {
            byte[] compressedData = fis.readAllBytes();
            byte[] decompressedData = decompress(compressedData);
            trie.deserialize(decompressedData);
            System.out.println("Metadata loaded successfully.");
            return trie;
        } catch (IOException e) {
            System.err.println("Error reading metadata from file: " + e.getMessage());
            return new TrieNode();
        }
    }

    public static void main(String[] args) {
        if (args.length > 3 && "--LF".equals(args[0]) && "--file".equals(args[2])) { // check syntax
            String metadataPath = args[1];
            String path = args[3];
            try {
                // Read entire file
                String content = new String(Files.readAllBytes(Paths.get(path)));

                Checker checker = new Checker(); // Run checker
                checker.analyze(content, metadataPath);
            } catch (IOException e) {
                System.err.println("Error reading file: " + e.getMessage());
            }
        } else {
            System.out.println("Invalid arguments. Usage: CLI --file [path]");
        }
    }
}
