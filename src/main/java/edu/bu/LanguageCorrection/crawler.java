package edu.bu.LanguageCorrection;

import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.*;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import javax.swing.JProgressBar;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.function.Consumer;

import java.lang.Math;

public class crawler {

    public static void main(String[] args) throws IOException {
        // Extract metadata file from first argument
        String file;
        if (!args[0].equals("--LF")) {
            System.err.println("Invalid argument. Please provide the metadata file.");
            return;
        } else {
            file = args[1];
        }


        // Initialize web crawler
        crawler web_crawler = new crawler(file);

        // Open the file and read all lines (URLs) into the queue
        String file_url = "";
        for (int i = 2; i < args.length; i++) {
            if (args[i].equals("--file")) {
                file_url = args[i + 1];
            } else if (args[i].equals("--debug")) {
                web_crawler.debug = true;
            } else if (args[i].equals("--build")) {
                System.out.println("Building off-corpus...");
                web_crawler.build(args[i + 1]);
            } else if (args[i].equals("--social")) {
                web_crawler.is_username = true;
                file_url = args[i + 1]; // Reuse variable to hold username
            }
        }
        if (!file_url.isEmpty()) {
            if (web_crawler.is_username) {
                String user_url = "https://www.reddit.com/user/" + file_url + "/"; // Convert username into link to user page
                web_crawler.add_to_queue(user_url);
            } else {
                try (FileReader f_read = new FileReader(file_url);
                        BufferedReader buf_read = new BufferedReader(f_read)) {
                    String url_line;
                    while ((url_line = buf_read.readLine()) != null) {
                        web_crawler.add_to_queue(url_line);
                    }
                }
            }
        }

        // Start crawling
        final int crawlLimit = 5; // Adjustable limit (SET TO 1 FOR EASE OF USE)
        web_crawler.crawl(crawlLimit);

        // Print url queue
        // web_crawler.get_queue();

        // Print visited URLs
        web_crawler.get_visited();
    }

    // public void get_queue(){
    // for (String url : url_queue) {
    // System.out.println(url);
    // }
    // }
    // members
    private final LinkedList<String> url_queue;
    private final HashSet<String> visited_urls;
    private String filePath;
    private TrieNode wordUsage = new TrieNode();
    private int compression_size = 0;
    private boolean debug = false; // flag that outputs uncompressed json showing the trie
    private boolean build_off_corpus = false;
    private boolean is_username = false; // flag for provided (reddit) username
    private static final int MAXNGRAM = 3;
    private Consumer<String> outputCallback;
    private JProgressBar progressBar;

    public crawler(String file) {
        url_queue = new LinkedList<>();
        visited_urls = new HashSet<>();

        // Load trie from file
        filePath = file;
        wordUsage = loadFile(filePath);
        this.outputCallback = System.out::println;

        // Estimate page count based on compressed file size
    }

    public crawler(String file, Consumer<String> outputCallback, JProgressBar progressBar) {
        this.outputCallback = outputCallback;
        this.progressBar = progressBar;
        url_queue = new LinkedList<>();
        visited_urls = new HashSet<>();

        // Load trie from file
        filePath = file;
        wordUsage = loadFile(filePath);
    }

    public void add_to_queue(String url) {
        url_queue.add(url);
    }

    public void crawl(int maxPages) {
        int pageCount = 0;
        while (!url_queue.isEmpty() && pageCount < maxPages) {
            String cur_site = url_queue.poll();

            // First 25 characters of url must match in order to be a reddit post
            String postCheck = "https://www.reddit.com/r/";
            boolean isPost = cur_site.contains(postCheck);

            if (cur_site == null || visited_urls.contains(cur_site)) {
                continue;
            }
            try {
                System.out.println("Processing: " + cur_site);
                outputCallback.accept("\n\nProcessing: " + cur_site);
                Document web_data = get_web_data(cur_site);
                if (web_data != null) {
                    processPage(web_data, isPost);
                    visited_urls.add(cur_site);
                    pageCount++;
                }
            } catch (IOException e) {
                System.err.println("Error processing " + cur_site + ": " + e.getMessage());
            }
        }
        System.out.println("Total pages visited: " + pageCount);
        outputCallback.accept("\nTotal pages visited: " + pageCount);
    }

    public void build(String language) {
        String corpus = "";
        if (language.equals("SmallEnglish")) {
            corpus = "brownSmall.txt";
        } else if (language.equals("English")) {
            corpus = "brown.txt";
        } else if (language.equals("German")) {
            corpus = "germanSmall.txt";
        } else if (language.equals("Italian")) {
            corpus = "italianSmall.txt";
        } else if (language.equals("Portuguese")) {
            corpus = "portugueseSmall.txt";
        } else {
            System.err.println("Unsupported language: " + language);
            return;
        }

        build_off_corpus = true;

        processPage(get_file_text(corpus), false);
    }

    private Document get_web_data(String url) throws IOException {
        // use execute() in order to receive a response object -> allows status code
        // checking
        Connection.Response req_response = Jsoup.connect(url)
                .userAgent(
                        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36")
                .referrer("http://www.google.com")
                .execute();

        // ensure that an OK is received
        if (req_response.statusCode() == 200) {
            return req_response.parse();
        } else {
            throw new HttpStatusException("Status not OK", req_response.statusCode(), url);
        }
    }

    private Document get_file_text(String filename) {
        // takes filename as input, reads it from src/main/resources and returns the
        // text as a Document object
        try {
            Document doc = new Document("");
            doc.title(filename);
            // Add each line to the document
            try (BufferedReader reader = new BufferedReader(new FileReader("src/main/java/resources/" + filename))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    doc.append(line);
                }
                // System.out.println("File read successfully.");
                outputCallback.accept("\nFile read successfully.");
            }
            return doc;
        } catch (IOException e) {
            System.err.println("Error reading file: " + e.getMessage());
            return null;
        }
    }

    private void processPage(Document web_data, boolean isPost) {
        byte[] compressedData = new byte[0];
        byte[] uncompressedData = new byte[0];
        boolean sizeLimitExceeded = false;
        byte[] previousUncompressedData = wordUsage.serialize();
        int foundLinks = 0;
        long startTime = System.nanoTime(),
                endTime;

        if (is_username) { // If a reddit username was passed to the command line
            is_username = false;
            Elements posts = web_data.select("shreddit-profile-comment[href]"); // Get posts from user profile
            // Add all posts in overview to url_queue
            for (Element link : posts) { // for links in the text
                String link_url = link.attr("href");
                // Strip the URL of any anchor tags or query tags
                if (link_url.contains("#"))
                    link_url = link_url.split("#")[0];
                if (link_url.contains("?"))
                    link_url = link_url.split("\\?")[0]; // ? is a special character in regex
                if (!link_url.isEmpty() && !visited_urls.contains(link_url)) {
                    foundLinks++;
                    url_queue.add(link_url);
                }
            }
        } else {
            Elements links = web_data.select("a[href]");
            for (Element link : links) {
                String link_url = link.attr("abs:href");
                // Strip the URL of any anchor tags
                if (link_url.contains("#"))
                    link_url = link_url.split("#")[0];
                if (link_url.contains("?"))
                    link_url = link_url.split("\\?")[0]; // ? is a special character in regex
                // System.out.println("Found link: " + link_url);
                if (!link_url.isEmpty() && !visited_urls.contains(link_url)) {
                    // System.out.println("Adding link to queue: " + link_url);
                    foundLinks++;
                    url_queue.add(link_url);
                }
            }
        }

        // Ignore for now - optimizes body + comment content retrieval
        // if(isPost) {
        // Get proper tags + queries
        // Element block =
        // web_data.selectFirst("shreddit-post.block.xs:mt-xs.xs:-mx-xs.xs:px-xs.xs:rounded-[16px].pt-xs.nd:pt-xs.bg-[color:var(--shreddit-content-background)].box-border.mb-xs.nd:visible.nd:pb-2xl");
        // String body = block.select("div.text-neutral-content.text-body").text();
        // System.out.println(body);
        // }

        if (!build_off_corpus) {
            // Break the page text into manageable chunks, considering sentences
            List<String> chunks = splitTextIntoChunks(web_data.text());
            if (chunks.isEmpty()) {
                // System.out.println("No text found on page.");
                outputCallback.accept("\nNo text found on page.");
                return;
            }
            // System.out.println(chunks.size());
            if (progressBar != null) {
                progressBar.setMaximum(1024);
                progressBar.setStringPainted(true);
            }

            for (String chunk : chunks) {
                // chunkCount++;
                // System.out.println(chunk);
                // System.out.println("Current compressed size: "+ compressedData.length+".
                // Processing chunk " + chunkCount + " of " + chunks.size());
                previousUncompressedData = uncompressedData.clone();
                extractWordUsage(chunk, wordUsage);
                uncompressedData = wordUsage.serialize();
                compressedData = compress(uncompressedData);
                if (progressBar != null) {
                    progressBar.setValue(compressedData.length - compression_size);
                }
                if ((compressedData.length - compression_size > 1024)) {
                    System.out.println("Previous size: " + compression_size
                            + " bytes. Current size: " + compressedData.length + " bytes. Delta: "
                            + (compressedData.length - compression_size) + " bytes.");
                    outputCallback.accept("\nPrevious compressed data size: " + compression_size
                            + " bytes. Current compressed data size: " + compressedData.length + " bytes. Delta: "
                            + (compressedData.length - compression_size) + " bytes.");
                    System.out.println("Size limit exceeded. Reverting to previous chunk.");
                    outputCallback.accept("\nSize limit exceeded. Reverting to previous chunk.");
                    sizeLimitExceeded = true;
                    uncompressedData = previousUncompressedData; // Revert to the previous uncompressed data
                    compressedData = compress(uncompressedData); // Recompress the reverted state
                    if (progressBar != null) {
                        progressBar.setValue(1024);
                    }
                    // Output # of links found in page
                    System.out.println("# of additional links found: " + foundLinks + "\n");
                    outputCallback.accept("\n# of additional links found: " + foundLinks + "\n");            
                    break; // Stop processing further chunks
                }
            }
        } else {
            if (progressBar != null) {
                progressBar.setMaximum(3);
                progressBar.setStringPainted(true);
                progressBar.setValue(0);
            }
            extractWordUsage(web_data.text().replaceAll("\\p{Punct}", ""), wordUsage);
            // System.out.println("Ngrams built successfully. for size:"+MAXNGRAM);
            if (progressBar != null)
                progressBar.setValue(1);
            uncompressedData = wordUsage.serialize();
            if (progressBar != null)
                progressBar.setValue(2);
            compressedData = compress(uncompressedData);
            if (progressBar != null) {
                progressBar.setValue(3);
                progressBar.setString("Done building inital trie");
            }
            build_off_corpus = false; // if there are urls to read it should still be able to read them
        }

        // Save the uncompressed and compressed data to separate files
        if (debug) {
            String uncompressedFilePath = "uncompressed-" + filePath.replace(".ser", ".json");
            writeToFile(uncompressedData, uncompressedFilePath);
            System.out.println("Uncompressed data exported successfully to: " + uncompressedFilePath);
        }

        writeToFile(compressedData, filePath);
        if (!sizeLimitExceeded) {
            System.out.println("Compressed tree exported successfully to: " + filePath);
            outputCallback.accept("\nCompressed tree exported successfully to: " + filePath);
        } else {
            System.out.println("Compressed data truncated due to size limit.");
        }
        endTime = System.nanoTime();

        // System.out.println((endTime - startTime)/1000000000.0); // Total time taken
        // to complete processing

        // Output sizes of both compressed and uncompressed data for reference
        System.out.println("Compressed metadata size: " + compressedData.length + " bytes");
        System.out.println("Uncompressed metadata size: " + uncompressedData.length + " bytes");
        outputCallback.accept("\nCompressed metadata size: " + compressedData.length + " bytes, Uncompressed metadata size: " + uncompressedData.length + " bytes");
        // Output rate of processing
        double processingRate = web_data.text().length() / ((endTime - startTime) / 1000000000.0);
        System.out.println("Rate of processing: " + Math.round(processingRate) + " bytes/second");
        outputCallback.accept("\nRate of processing: " + Math.round(processingRate) + " bytes/second");
        compression_size = compressedData.length;
    }

    private List<String> splitTextIntoChunks(String text) {
        String[] sentences = text.split("[.!?\n] ");
        List<String> chunks = new ArrayList<>();
        for (String sentence : sentences) {
            if (sentence.length() > 100) {
                continue;
            }
            chunks.add(sentence.replaceAll("\\p{Punct}", ""));
        }
        return chunks;
    }

    private static void extractWordUsage(String text, TrieNode trie) {
        // Split text into sentences
        String[] sentences = text.split("[.!?] ");
        for (String sentence : sentences) {
            for (int nGram = 1; nGram <= MAXNGRAM; nGram++) {
                String[] words = sentence.split("\\s+");
                for (int i = 0; i < words.length - nGram + 1; i++) {
                    trie.insert(Arrays.copyOfRange(words, i, i + nGram));
                }
            }
        }
    }

    private static byte[] compress(byte[] input) {
        // Create compressor
        Deflater compressor = new Deflater(Deflater.BEST_COMPRESSION);
        compressor.setInput(input);
        compressor.finish();

        // Store compressed data in dynamic byte array
        ByteArrayOutputStream bos = new ByteArrayOutputStream(input.length);

        byte[] buf = new byte[1024];
        while (!compressor.finished()) {
            int count = compressor.deflate(buf);
            bos.write(buf, 0, count);
        }
        compressor.end();

        return bos.toByteArray();
    }

    private static byte[] decompress(byte[] compressedData) {
        Inflater decompressor = new Inflater();
        decompressor.setInput(compressedData);

        ByteArrayOutputStream bos = new ByteArrayOutputStream(compressedData.length);

        byte[] buf = new byte[1024];
        try {
            while (!decompressor.finished()) {
                int count = decompressor.inflate(buf);
                bos.write(buf, 0, count);
            }
            decompressor.end();
            return bos.toByteArray();
        } catch (Exception e) {
            System.err.println("Error decompressing data: " + e.getMessage());
            return new byte[0];
        }
    }

    private TrieNode loadFile(String filePath) {
        TrieNode trie = new TrieNode();
        try (FileInputStream fis = new FileInputStream(filePath)) {
            byte[] compressedData = fis.readAllBytes();
            byte[] decompressedData = decompress(compressedData);
            compression_size = compressedData.length;
            // System.out.println("Compressed metadata size: " + compressedData.length + "
            // bytes");
            // System.out.println("Decompressed metadata size: " + decompressedData.length +
            // " bytes");
            trie.deserialize(decompressedData);
            System.out.println("Metadata loaded successfully.");
            return trie;
        } catch (IOException e) {
            // System.err.println("Error reading metadata from file: " + e.getMessage());
            System.err.println("Creating new trie...");
            return new TrieNode();
        }
    }

    private static void writeToFile(byte[] compressedData, String filePath) {
        try (FileOutputStream fos = new FileOutputStream(filePath)) {
            fos.write(compressedData);
        } catch (IOException e) {
            System.err.println("Error writing metadata to file: " + e.getMessage());
        }
    }

    // Output visited URLs as specified
    public void get_visited() {
        System.out.println("All of the visited websites:");
        for (String url : visited_urls) {
            System.out.println(url);
        }
    }
}
