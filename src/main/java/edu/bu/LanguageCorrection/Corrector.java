package edu.bu.LanguageCorrection;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.zip.Inflater;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.function.Consumer;
import java.util.Collections;
import java.util.HashMap;


public class Corrector {
    private TrieNode detector;
    private static Consumer<String> outputCallback;

    public Corrector(String metadataPath) {
        detector = loadFile(metadataPath);
        outputCallback = System.out::println;
    }

    public void setCallback(Consumer<String> callback) {
        outputCallback = callback;
    }

    public TrieNode getDetector() {
        return detector;
    }

    private TrieNode loadFile(String filePath) {
        TrieNode trie = new TrieNode();
        try (FileInputStream fis = new FileInputStream(filePath)) {
            byte[] compressedData = fis.readAllBytes();
            byte[] decompressedData = decompress(compressedData);
            trie.deserialize(decompressedData);
            System.out.println("Metadata loaded successfully.");
            return trie;
        } catch (IOException e) {
            System.err.println("Error reading metadata from file: " + e.getMessage());
            return new TrieNode();
        }
    }

    private static byte[] decompress(byte[] compressedData) {
        Inflater decompressor = new Inflater();
        decompressor.setInput(compressedData);

        ByteArrayOutputStream bos = new ByteArrayOutputStream(compressedData.length);

        byte[] buf = new byte[1024];
        try {
            while (!decompressor.finished()) {
                int count = decompressor.inflate(buf);
                bos.write(buf, 0, count);
            }
            decompressor.end();
            return bos.toByteArray();
        } catch (Exception e) {
            System.err.println("Error decompressing data: " + e.getMessage());
            return new byte[0];
        }
    }

    public String[] correct(String inputSentence) {
        // Divide sentence into words
        String[] words = inputSentence.split(" ");

        // Check if all words are in the trie
        for (int i = 0; i < words.length; i++) {
            if (!detector.containsWord(words[i])) {
                String closestWord = detector.findClosestWord(words[i]);
                if (closestWord != null) {
                    words[i] = closestWord;
                }
            }
        }

        List<String> sentences = generateSentences(words);

        // Use a priority queue to store sentences along with their scores
        PriorityQueue<SentenceScorePair> pq = new PriorityQueue<>(
                Collections.reverseOrder());

        for (String sentence : sentences) {
            float score = detector.perplexity(sentence);
            // System.out.println(sentence + " | Score: " + score);

            pq.offer(new SentenceScorePair(sentence, score));

            // Ensure only the top 5 sentences are kept, remove the worst if more than 5
            if (pq.size() > 5) {
                pq.poll();
            }
        }

        // Print the top sentences with their scores
        SentenceScorePair[] topPairs = new SentenceScorePair[pq.size()];
        int index = pq.size() - 1;
        while (!pq.isEmpty()) {
            topPairs[index] = pq.poll();
            index--;
        }

        // Print sentences in the right order
        // for (SentenceScorePair pair : topPairs) {
        //     System.out.println(pair.sentence + " | Score: " + pair.score);
        // }

        // Convert to array of just sentences
        String[] topSentences = new String[topPairs.length+1];
        topSentences[0] = inputSentence+" | Score: "+detector.perplexity(inputSentence);
        for (int i = 1; i < topPairs.length+1; i++) {
            // Remove sentence that are not in the 0.5 percentile of the best sentence
            if (topPairs[i-1].score > topPairs[0].score * 1.5) {
                topSentences[i] = "";
            } else {
                topSentences[i] = topPairs[i-1].sentence + " | Score: " + topPairs[i-1].score;
            }
        }

        // Check if the original sentence is in the top 5
        boolean originalInTop5 = false;
        boolean first = true;
        for (String sentence : topSentences) {
            if (first) {
                first = false;
                continue;
            }
            sentence = sentence.split(" \\| Score: ")[0]; // Remove score
            //System.out.println(sentence);
            if (sentence != null && sentence.equals(inputSentence)) {
                originalInTop5 = true;
                break;
            }
        }

        if (originalInTop5) {
            String[] result = new String[1];
            result[0] = topSentences[0];
            return result;
        } else {
            return topSentences;
        }
    }

    // Helper class to manage sentences and their scores
    class SentenceScorePair implements Comparable<SentenceScorePair> {
        String sentence;
        float score;

        public SentenceScorePair(String sentence, float score) {
            this.sentence = sentence;
            this.score = score;
        }

        @Override
        public int compareTo(SentenceScorePair other) {
            return Float.compare(this.score, other.score);
        }
    }

    public static List<String> generateSentences(String[] words) {
        List<String> results = new ArrayList<>();
        boolean[] used = new boolean[words.length];
        backtrack(results, words, new ArrayList<>(), used);
        return results;
    }

    private static void backtrack(List<String> results, String[] words, List<String> current, boolean[] used) {
        if (current.size() >= Math.ceil(words.length * 3.0 / 4.0) && current.size() <= words.length) {
            results.add(String.join(" ", current));
        }

        for (int i = 0; i < words.length; i++) {
            if (used[i])
                continue; // Skip used words

            used[i] = true;
            current.add(words[i]);
            backtrack(results, words, current, used);
            current.remove(current.size() - 1);
            used[i] = false;
        }
    }

    private static int longestCommonSubsequence(String[] originalWords, String[] shuffledWords) {
        int n = originalWords.length;
        int m = shuffledWords.length;
        int[][] dp = new int[n + 1][m + 1];

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                if (originalWords[i - 1].equals(shuffledWords[j - 1])) {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                } else {
                    dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
                }
            }
        }
        return dp[n][m];
    }

    public void printSentencesInOrderOfChanges(String[] sentences, String originalSentence) {
        // Order the sentences by the number of changes needed
        Map<String, Integer> changesMap = new HashMap<>();

        String[] originalWords = originalSentence.split(" ");
        boolean first = true;

        for (String sentence : sentences) {
            if (sentence == null || sentence.isEmpty() || first) {
                first = false;
                continue;
            }

            String sentencePart = sentence.split(" \\| Score: ")[0]; // Remove score, if present
            String[] shuffledWords = sentencePart.split(" ");

            // Calculate the longest common subsequence
            int lcsLength = longestCommonSubsequence(originalWords, shuffledWords);

            // Number of changes needed is the difference in length minus LCS length
            int changes = (originalWords.length - lcsLength) + (shuffledWords.length - lcsLength);

            changesMap.put(sentence, changes);
        }

        // Sort by number of changes in ascending order
        List<Map.Entry<String, Integer>> sortedList = new ArrayList<>(changesMap.entrySet());
        sortedList.sort(Map.Entry.comparingByValue());


        for (Map.Entry<String, Integer> entry : sortedList) {
            outputCallback.accept(">> " + entry.getKey() + " | Changes: " + entry.getValue()+"\n");
        }
    }

    public static void main(String[] args) {
        if (args.length > 3 && "--LF".equals(args[0]) && "--file".equals(args[2])) {
            String metadataPath = args[1];
            String path = args[3];
            try {
                String content = new String(Files.readAllBytes(Paths.get(path)));
                Corrector corrector = new Corrector(metadataPath); // Run corrector
                corrector.setCallback(System.out::println);
                String[] sentences = TextProcessor.extractSentences(content).toArray(new String[0]);
                for (String sentence : sentences) {
                    sentence = sentence.replaceAll("[^a-zA-Z0-9\\s]", "");
                    String[] corrected = corrector.correct(sentence);
                    if (corrected.length == 0) {
                        outputCallback.accept(sentence + " | No corrections needed.\n");
                        continue;
                    }
                    outputCallback.accept(sentence + " | Corrected Sentence Suggestions:");
                    corrector.printSentencesInOrderOfChanges(corrected, sentence);
                }
            } catch (IOException e) {
                System.err.println("Error reading file: " + e.getMessage());
            }
        } else {
            System.out.println("Invalid arguments. Usage: CLI --file [path]");
        }
    }
}
