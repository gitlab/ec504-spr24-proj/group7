package edu.bu.LanguageCorrection;

import javax.swing.*;

// import java.awt.event.ActionEvent;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.text.*;

import scala.collection.mutable.StringBuilder;

import java.awt.Color;
import java.awt.BorderLayout;
import java.util.zip.Deflater;
import java.io.FileOutputStream;
import java.io.IOException;



public class MainApp extends JFrame {
    private final JTextField urlField;
    private JTextArea resultArea;
    private final JButton runButton;
    private final JButton changeLanguageButton;
    private final JComboBox<String> moduleSelector;
    private JProgressBar progressBar = new JProgressBar(0, 100);

    private String languageFile = "SmallEnglish.ser";

    public MainApp() {
        super("Language Correction Tool");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(800, 600);
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());

        // Check if metadata file exists
        if (!Files.exists(Paths.get(languageFile))) {
            String[] languages = {"SmallEnglish","English", "German", "Portuguese", "Italian"};
            String selectedLanguage = (String) JOptionPane.showInputDialog(this, "Metadata file not found. Please choose a language to build off of. \n(If you want to build from scratch just click Cancel)", "Language Selection", JOptionPane.PLAIN_MESSAGE, null, languages, languages[0]);
            if (selectedLanguage != null) {
                runBuilder(selectedLanguage);
            }
        }

        // Module selector
        String[] modules = {"Select Module", "Web Crawler", "Reddit Crawler", "File Checker", "File Corrector", "Text Checker", "Text Corrector"};
        moduleSelector = new JComboBox<>(modules);

        // User inout entry field and run button
        urlField = new JTextField();
        runButton = new JButton("Run");
        runButton.addActionListener(e -> {
            String selectedModule = (String) moduleSelector.getSelectedItem();
            String input = urlField.getText().trim();

            switch (selectedModule) {
                case "Web Crawler":
                    runCrawler(input,10);
                    break;
                case "Reddit Crawler":
                    runCrawler("https://www.reddit.com/r/"+input, 1);
                    break;
                case "File Checker":
                    runChecker(input,true);
                    break;
                case "File Corrector":
                    runCorrector(input,true);
                    break;
                case "Text Checker":
                    runChecker(input,false);
                    break;
                case "Text Corrector":
                    runCorrector(input,false);
                    break;
                default:
                    JOptionPane.showMessageDialog(this, "Select a valid module");
            }
        });
        urlField.addActionListener(e -> runButton.doClick()); // On enter keydown it will click run
        changeLanguageButton = new JButton("Change Language");
        changeLanguageButton.addActionListener(e -> {
            String[] languages = {"English", "German", "Portuguese", "Italian"};
            String selectedLanguage = (String) JOptionPane.showInputDialog(this, "Please choose a language to build off of.", "Language Selection", JOptionPane.PLAIN_MESSAGE, null, languages, languages[0]);
            if (selectedLanguage != null) {
                languageFile = selectedLanguage + ".ser";

                // Check if metadata file exists
                String extraText = "";
                if (!Files.exists(Paths.get(languageFile))) {
                    runBuilder(selectedLanguage);
                    extraText = "New metadata file created.\n";
                }

                JOptionPane.showMessageDialog(this, extraText + "Language changed to " + selectedLanguage + ".");
            }
        });

        JPanel northPanel = new JPanel(new BorderLayout());
        northPanel.add(moduleSelector, BorderLayout.WEST);
        northPanel.add(urlField, BorderLayout.CENTER);
        northPanel.add(runButton, BorderLayout.EAST);
        northPanel.add(changeLanguageButton, BorderLayout.NORTH);
        northPanel.add(progressBar, BorderLayout.SOUTH);

        // Result area
        resultArea = new JTextArea();
        resultArea.setEditable(false);
        JScrollPane scrollPane = new JScrollPane(resultArea);

        add(northPanel, BorderLayout.NORTH);
        add(scrollPane, BorderLayout.CENTER);
    }

    private void runBuilder(String selectedLanguage) {
        languageFile = selectedLanguage + ".ser";
        StringBuilder outputBuilder = new StringBuilder();
        crawler webCrawler = new crawler(languageFile, output -> {
            outputBuilder.append(output);
            SwingUtilities.invokeLater(() -> resultArea.setText(outputBuilder.toString()));
        }, progressBar);
        Thread builderThread = new Thread(() -> {
            webCrawler.build(selectedLanguage);
        });
        progressBar.setString("Building "+selectedLanguage+" metadata file...");
        progressBar.setValue(0);
        builderThread.start();
    }
    
    private void runCrawler(String input, Integer limit) {
        progressBar.setValue(0);
        progressBar.setString(null);
        StringBuilder outputBuilder = new StringBuilder();
        crawler webCrawler = new crawler(languageFile, output -> {
            outputBuilder.append(output);
            SwingUtilities.invokeLater(() -> resultArea.setText(outputBuilder.toString()));
        }, progressBar);
        if (input.startsWith("http")) {
            // Input is a single URL
            webCrawler.add_to_queue(input);
        } else {
            // Input is file path
            try {
                List<String> lines = Files.readAllLines(Paths.get(input));
                lines.forEach(webCrawler::add_to_queue);
            } catch (Exception e) {
                resultArea.setText("Error reading file/link. Please make sure to include http or .txt for link or file respectively: " + e.getMessage());
                return;
            }
        }
        // Create a new thread for running the crawler
        Thread crawlerThread = new Thread(() -> {
            webCrawler.crawl(limit); // Adjust the limit (DEFAULT SET TO 1)
        });
        // Start the crawler thread
        crawlerThread.start();
    }

    private void runChecker(String text, boolean isFile) {
        try {
            String content;
            if (isFile) {
                content = new String(Files.readAllBytes(Paths.get(text)));
            } else {
                content = text.toLowerCase();
            }

            // Assume content is already properly split into sentences here
            List<String> sentences = TextProcessor.extractSentences(content); // Use a method to split into sentences

            Checker checker = new Checker();
            StringBuilder result = new StringBuilder();

            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            PrintStream printStream = new PrintStream(outputStream);
            PrintStream originalOut = System.out;

            String[] worstPhrases = new String[sentences.size()];

            for (String sentence : sentences) {
                System.setOut(printStream);
                checker.analyze(sentence, languageFile); // Analyze each sentence separately

                // Reset System.out
                System.out.flush();
                System.setOut(originalOut);

                // Capture the output into a string
                String output = outputStream.toString();
                outputStream.reset(); // Clear the output stream for the next sentence
                //System.out.println(output);

                // Parsing the output to get phrases and their scores
                String[] lines = output.split("\n");
                double lowestScore = Double.MAX_VALUE;
                String worstPhrase = null;

                for (String line : lines) {
                    if (line.startsWith("\"phrases\":")) {
                        //System.out.println(line);
                        line = line.replace("\"phrases\":", "").replace("{", "").replace("}", "").trim();
                        //System.out.println(line);
                        String[] phrases = line.split(",");
                        //System.out.println(line);
                        for (String phrase : phrases) {
                            String[] parts = phrase.trim().split(":");
                            double phraseScore = parts[1].trim().equals("null") ? 0
                                    : Double.parseDouble(parts[1].trim());
                            if (phraseScore < lowestScore) {
                                lowestScore = phraseScore;
                                worstPhrase = parts[0].trim();
                                worstPhrases[sentences.indexOf(sentence)] = worstPhrase;
                            }
                        }
                    }
                }

                if (worstPhrase != null && lowestScore > 10.0) { // Check if the worst phrase has a low enough score
                    // Append to the result with annotations
                    result.append("\nSentence: ").append(sentence)
                            .append("\n>> Worst Phrase: ").append(worstPhrase)
                            .append(" (Score: ").append(lowestScore).append(")\n");
                } else {
                    result.append("\nSentence: ").append(sentence)
                            .append("\n>> No worst phrase found (with low enough score), the sentence is grammatically correct.\n");
                    worstPhrases[sentences.indexOf(sentence)] = "";
                }
            }

            resultArea.setText(result.toString()); // Display the annotated results in the JTextArea

            // Highlight the worst phrase in each sentence
            Highlighter highlighter = resultArea.getHighlighter();
            Highlighter.HighlightPainter painter = new DefaultHighlighter.DefaultHighlightPainter(Color.YELLOW);
            for (String phrase : worstPhrases) {
                phrase = phrase.replaceAll("\"", "");
                //System.out.println(phrase);
                if (phrase != null && !phrase.isEmpty()) {
                    //System.out.println(resultArea.getText());
                    int start = resultArea.getText().indexOf(phrase);
                    if (start != -1) {
                        int end = start + phrase.length();
                        try {
                            highlighter.addHighlight(start, end, painter);
                        } catch (BadLocationException e) {
                            e.printStackTrace();
                        }
                    } else {
                        System.out.println("Text not found");
                    }
                }
            }
        } catch (Exception e) {
            //throw new RuntimeException(e);
            resultArea.setText("Error: " + e.getMessage());
        }
    }

    private void runCorrector(String input, boolean isFile) {
        try {
            String content;
            if (isFile) 
                content= new String(Files.readAllBytes(Paths.get(input)));
            else
                content = input.toLowerCase();
            Corrector corrector = new Corrector(languageFile);
            Consumer<String> outputCallback = output -> {
                resultArea.append(output);
                resultArea.setCaretPosition(resultArea.getDocument().getLength());
            };
            corrector.setCallback(outputCallback);
            String[] sentences = TextProcessor.extractSentences(content).toArray(new String[0]);
            progressBar.setValue(0);
            progressBar.setStringPainted(true);
            progressBar.setString(null);
            progressBar.setMaximum(sentences.length*2);
            for (String sentence : sentences) {
                progressBar.setValue(progressBar.getValue() + 1);
                sentence = sentence.replaceAll("[^a-zA-Z0-9\\s]", "");
                String[] corrected = corrector.correct(sentence);
                progressBar.setValue(progressBar.getValue() + 2);
                if (corrected.length == 1) {
                    outputCallback.accept(sentence + " | No corrections found for this sentence.\n\n");
                    continue;
                }
                outputCallback.accept(corrected[0] + " | Corrected Sentence Suggestions:\n");
                corrector.printSentencesInOrderOfChanges(corrected, sentence);
            }
            // resultArea.setText(result.toString());
            // ISSUE #30 - Feedback for corrector
            // Add a pop up to input the best correction for each sentence
            TrieNode node = corrector.getDetector();
            boolean changeMade = false;
            for (String sentence : sentences) {
                String sentenceNew = sentence.replaceAll("[^a-zA-Z0-9\\s]", "");
                String[] corrected = corrector.correct(sentenceNew);
                int correctedLength = corrected.length;
                if (correctedLength <= 1) {
                    continue;
                }
                for (String s : corrected) {
                    if (s == null || s.isEmpty()) {
                        correctedLength--;
                    }
                }
                //System.out.println(correctedLength);
                String[] options = new String[correctedLength+1];
                for (int i = 0; i < corrected.length; i++) {
                    // System.out.println(corrected[i]);
                    options[i] = corrected[i];
                    // System.out.println(options[i]);
                    options[i] = options[i].split(" \\| Score: ")[0];
                    // System.out.println(options[i]);
                }
                String selected = (String) JOptionPane.showInputDialog(this, "Select the best correction for the following sentence:\n  " + sentence, "Correction Selection", JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
                if (selected != null) {
                    progressBar.setValue(0);
                    progressBar.setStringPainted(true);
                    progressBar.setString(null);
                    // Insert the selected correction into the trie for future reference
                    List<String> phrases = TextProcessor.extractPhrases(selected, 1, 3);
                    progressBar.setMaximum(phrases.size());
                    for (String phrase : phrases) {
                        String[] words = phrase.split(" ");
                        node.insert(words);
                        progressBar.setValue(progressBar.getValue() + 1);
                    }
                    changeMade = true;
                    resultArea.setText("");
                } else {
                    System.out.println("No correction selected");
                }
            }

            // Serialize the trie
            if (changeMade) {
                byte[] serialized = node.serialize();
                byte[] compressed = compress(serialized);
                writeToFile(compressed, languageFile);
            }
        } catch (Exception e) {
            resultArea.setText("Error: " + e.getMessage());
        }
    }

    private static byte[] compress(byte[] input) {
        // Create compressor
        Deflater compressor = new Deflater(Deflater.BEST_COMPRESSION);
        compressor.setInput(input);
        compressor.finish();

        // Store compressed data in dynamic byte array
        ByteArrayOutputStream bos = new ByteArrayOutputStream(input.length);

        byte[] buf = new byte[1024];
        while (!compressor.finished()) {
            int count = compressor.deflate(buf);
            bos.write(buf, 0, count);
        }
        compressor.end();

        return bos.toByteArray();
    }

    private static void writeToFile(byte[] compressedData, String filePath) {
        try (FileOutputStream fos = new FileOutputStream(filePath)) {
            fos.write(compressedData);
        } catch (IOException e) {
            System.err.println("Error writing metadata to file: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            MainApp mainApp = new MainApp();
            mainApp.setVisible(true);
        });
    }
}
