package edu.bu.LanguageCorrection;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

public class TrieNode implements Serializable, Cloneable {
    HashMap<String, TrieNode> children = new HashMap<>();
    int count = 0;
    int childCounts = 0;
    String seperator = " ";

    public void insert(String[] phrase) {
        TrieNode current = this;
        TrieNode past = this; // Store the previous node
        for (String word : phrase) {
            if (word.length() == 0) {
                continue;
            }
            past = current;
            current = current.children.computeIfAbsent(word.toLowerCase(), c -> new TrieNode());
        }
        current.count += 1;
        past.childCounts += 1;
    }

    public boolean containsWord(String word) {
        return this.children.containsKey(word);
    }

    public String findClosestWord(String inputWord) {
        int minDistance = Integer.MAX_VALUE;
        String closestWord = null;

        for (String word : this.children.keySet()) {
            int distance = levenshteinDistance(inputWord, word);
            if (distance < minDistance) {
                minDistance = distance;
                closestWord = word;
            }
        }
        return closestWord;
    }

    private int levenshteinDistance(String s1, String s2) {
        int[][] dp = new int[s1.length() + 1][s2.length() + 1];

        for (int i = 0; i <= s1.length(); i++) {
            for (int j = 0; j <= s2.length(); j++) {
                if (i == 0) {
                    dp[i][j] = j;
                } else if (j == 0) {
                    dp[i][j] = i;
                } else {
                    dp[i][j] = Math.min(Math.min(dp[i - 1][j] + 1, dp[i][j - 1] + 1),
                            dp[i - 1][j - 1] + (s1.charAt(i - 1) == s2.charAt(j - 1) ? 0 : 1));
                }
            }
        }
        return dp[s1.length()][s2.length()];
    }

    public float probability(String phrase) {
        String[] words = phrase.split(" ");
        if (words.length <= 1) { // If word does not exist in trie
            // System.out.println("Probability of phrase: " + 1 / this.childCounts);
            return (float) 0.1;
        }
        TrieNode current = this;
        TrieNode past = this;
        // System.out.println("Phrase: " + phrase);
        for (String word : words) {
            past = current;
            current = current.children.get(word);
            if (current == null) {
                float alpha = (float) 1;
                // System.out.println("Phrase not found in trie.");
                return alpha * probability(phrase.substring(phrase.indexOf(" ") + 1));
            }
        }
        // System.out.println("Probability of phrase: " + (float) current.count / past.childCounts);
        return (float) current.count / past.childCounts;
    }

    private float getAverageChildCount(TrieNode node) {
        return (float) node.childCounts / node.children.size();
    }

    private int factorial(int n) {
        int res = 1, i;
        for (i = 2; i <= n; i++)
            res *= i;
        return res;
    }

    public float perplexity(String sentence) {
        List<String> phrases = TextProcessor.extractPhrases(sentence, 2, 3);
        float perplexity = 0;

        for (String phrase : phrases) {
            float logProb = 0;
            String[] words = phrase.split(" ");
            String currentPhrase = "";
            for (String word : words) {
                if (currentPhrase.length() == 0) {
                    currentPhrase = word;
                } else {
                    currentPhrase += " " + word;
                }
                logProb += Math.log((probability(currentPhrase)));
            }
            perplexity += (float) Math.pow(2, -logProb);
            // System.out.println("Perplexity of phrase (" + phrase + ") : " + ((float)
            // perplexity / factorial(words.length)));
        }

        
        return (float) perplexity / phrases.size();
    }
    
    public byte[] serialize() {
        return serializeHelper(this).replaceAll(seperator+"\\}"+seperator, "\\}").replaceAll(seperator+"\\{"+seperator, "\\{").trim().getBytes();
    }

    private String serializeHelper(TrieNode node) {
        StringBuilder serialized = new StringBuilder();
        serialized.append("\"").append(node.count).append(",").append(node.childCounts).append("\"");
        if (node.childCounts == 0) {
            serialized.append(seperator);
            // System.out.println("Serialized node: " + node.count + " " + node.childCounts);
            return serialized.toString();
        } else {
            serialized.append("{");
        }
        for (String key : node.children.keySet()) {
            // System.out.println(" key: " + key);
            serialized.append("\""+key+"\":").append(seperator).append(serializeHelper(node.children.get(key)));
        }
        serialized.append("},");
        // System.out.println("Serialized node: " + node.count + " " + node.childCounts);

        return serialized.toString();
    }

    public void deserialize(byte[] data) {
        String serializedString = new String(data);
        // Ensure proper spacing around '{' and '}' to correctly parse children
        serializedString = serializedString.replaceAll("\\{", seperator+"\\{"+seperator).replaceAll("\\}", "\\}").trim();
        serializedString = serializedString.replaceAll("\"", "").replaceAll(":", "").replaceAll(",", " ");
        String[] parts = serializedString.split(seperator);
        int[] index = { 0 };

        // Clear current state before rebuilding
        this.children.clear();
        deserializeHelper(this, parts, index);
    }

    // Helper method adapted for in-place deserialization
    private static void deserializeHelper(TrieNode node, String[] parts, int[] index) {
        if (index[0] >= parts.length) {
            throw new IllegalArgumentException("Unexpected end of serialized data.");
        }

        // Set the current node's count and childCounts
        // System.out.println(parts[index[0]]);
        node.count = Integer.parseInt(parts[index[0]++]);
        // System.out.println(parts[index[0]]);
        node.childCounts = Integer.parseInt(parts[index[0]++]);
        // System.out.println(parts[index[0]]);

        if ("{".equals(parts[index[0]])) {
            index[0]++; // Move past the "{" marker
            while (!"}".equals(parts[index[0]])) {
                String key = parts[index[0]++];
                // System.out.println("Key: " + key);
                TrieNode child = new TrieNode();
                deserializeHelper(child, parts, index);
                node.children.put(key, child);
            }
            index[0]++; // Skip the "}" marker
        }
    }

    @Override
    public TrieNode clone() {
        try {
            TrieNode clonedNode = (TrieNode) super.clone();
            clonedNode.children = new HashMap<>();
            for (Map.Entry<String, TrieNode> child : this.children.entrySet()) {
                // Recursively clone and add each child to the cloned node
                clonedNode.children.put(child.getKey(), child.getValue().clone());
            }
            // Count and childCounts are primitive types, so they're already correctly
            // copied by super.clone()
            return clonedNode;
        } catch (CloneNotSupportedException e) {
            // This should not happen since we're Cloneable
            throw new AssertionError(e);
        }
    }
}
