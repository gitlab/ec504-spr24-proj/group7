import requests
from bs4 import BeautifulSoup

def get_wikipedia_article_links(url):
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    
    links = []
    for link in soup.find_all('a', href=True):
        if '/wiki/' in link['href'] and ':' not in link['href']:
            full_link = f"https://en.wikipedia.org{link['href']}"
            if full_link not in links:
                links.append(full_link)
    
    return links

# Example: Fetching links from the Unusual Articles page
unusual_articles_url = 'https://en.wikipedia.org/wiki/Wikipedia:Unusual_articles'
unusual_links = get_wikipedia_article_links(unusual_articles_url)

# Print the first 10 links as a sample
for link in unusual_links[:100]:
    print(link)
