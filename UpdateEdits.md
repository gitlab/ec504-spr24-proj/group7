## Accepted Changes

### Change 1: Display Full Language Name in Dropdown Menu (Issue #25)
The change was adopted as shortened language names could be ambiguous to the user. The full language name is now displayed in the dropdown menu.

### Change 3: Fix incorrect spelling of words in corrector (Issue #27)
The corrector now correctly identifies and suggests corrections for misspelled words. This change improves the corrector's accuracy and provides the user with more accurate suggestions. It does this by looking at the Levenshtein Distance between the misspelled word and the words in the trie, and suggesting the word with the smallest distance.

### Change 4: Corrector and Checker no longer suggest changes when the sentence is correct (Issue #26)
The corrector and checker no longer suggest changes when the sentence is correct. This change improves the user experience by reducing the number of unnecessary suggestions.

### Change 5: Added executable bash script for running the application (Issue #20)
The change was adopted to make it easier for users to run the application. The bash script `runVisual.sh` was added to the repository, which allows users to run the GUI application with a single command. This change improves the user experience by making it easier for users to run the application. Other bash scripts were also added to run the crawler, corrector, and checker separately in CLI.

## Other Changes

### Change 1: Crawler Progress Bar
In order to fully implement Issue #11 we wanted to add a progress bar to show how much of the initial corpus has been scanned vs how much is left. This involves moving the build function into a seperate thread and passing the `JProgressBar` in to the crawler. 

### Change 2: Pop-up Correction Feedback Selection
The implemented pop-up gives the user the ability to choose which corrections provided by the corrector is best. This correction is then added to the trie node and used for future suggestions. This allows the user to have more control over the suggestions provided by the corrector and improves the user experience by making the corrector more accurate.

### Change 3: Language Selection in GUI
The language selection in the GUI was also added to make the user experience more intuitive and allow them to easily change the language without having to shut down the application. In order to implement this change, the language selection was added to the GUI, and the corrector, checker, and crawler were updated to receive the language specific metadata file as an argument.

### Change 4: Improved crawler printing
The text printed by the Crawler module wasn't that straightforward to read so we cleaned it up substantially.

## Rejected Changes
We only decided to reject Issue #22 because we didn't think that selecting a file was that necessary compared to the other changes we made. However, there were some issues raised due to miss use of the application (which were: #23, #24, and #28). These issues were resolved by providing more information on how to use the application in the README file.
